import json
import socket
import sys

import game_state
import game_ai
import data_mapper

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.car_settings = game_ai.CarSettings()
        self.previous_throttle = 0
        self.car_history = game_state.CarHistory()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    def advanced_join(self):
        return self.msg("createRace", {"botId": {"name": self.name,
                                 "key": self.key},
                                 "trackName": "france",
                                 "password": "asdf34aefaefw23",
                                 "carCount": 1})        

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def use_turbo(self, message):
        print("Used turbo : %s" % (message))
        self.car_settings.turbo_available = False
        self.car_settings.turbo = game_state.Turbo(0.0,0,0.0)
        self.msg("turbo", message)     

    def run(self):
        self.join()
#        self.advanced_join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        car_position = data_mapper.read_car_position(data, self.name)
        self.car_history.add_position(car_position)
        current_speed = self.car_history.current_speed()
        throttle = game_ai.set_throttle(car_position, current_speed, self.track, self.car_settings)

        if self.track.does_lap_have_curves_left(car_position) == False and self.car_settings.turbo_available :
            self.use_turbo("WROOOOOOM!")

        # debug
        if (self.previous_throttle != throttle):
            print("throttle changed")
            print(throttle)
            print(car_position)
            self.previous_throttle = throttle

        self.throttle(throttle)

    def on_crash(self, data):
        print("Someone crashed")
        driver = data_mapper.get_crashed_car(data)
        if (driver == self.name):
            print("Your car crashed")
            current_position = self.car_history.current_position()
            current_piece = self.track.get_piece(current_position.piece_index)
            current_speed = self.car_history.current_speed()
            game_ai.adjust_crashed_car_settings(current_piece, current_speed, self.car_settings)
            print(self.car_settings)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        self.track = data_mapper.read_track(data)
        print(self.track)
        self.ping()

    def on_your_car(self, data):
        print("Your car")
        self.ping()

    def on_turbo_available(self, data):
        print("Turbo available")
        turbo = data_mapper.read_turbo(data)
        print(turbo)
        self.car_settings.turbo = turbo
        self.car_settings.turbo_available = True
        self.ping()

    def on_lap_finished(self, data):
        print("Lap finished")
        #print(self.car_history)
        self.car_history.current_lap = self.car_history.current_lap + 1
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'turboAvailable': self.on_turbo_available,
            'lapFinished': self.on_lap_finished
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
