import math

def arc_length(radius, angle):
    return 2 * math.pi * radius * (math.fabs(angle) / 360)

class CarPosition:
    def __init__(self, piece_index, in_piece_distance, angle):
        self.piece_index = piece_index
        self.in_piece_distance = in_piece_distance
        self.angle = angle

    def __str__(self):
        return 'piece index: {0}, in piece distance: {1}, angle: {2}'.format(self.piece_index, self.in_piece_distance, self.angle)

class Piece:
    def __init__(self, length, radius, angle):
        self.is_curved = False
        self.radius = 0
        self.length = 0
        self.angle = 0

        if (length > 0):
            self.length = length
        else:
            self.is_curved = True
            self.radius = radius
            self.angle = angle
            self.length = arc_length(radius, angle)

    def __str__(self):
        return 'length: {0}, angle: {1}, radius: {2}, is curved: {3}'.format(self.length, self.angle, self.radius, self.is_curved)

class Track:
    def __init__(self):
        self.pieces = []

    def add_piece(self, piece):
        self.pieces.append(piece)

    def get_piece(self, index):
        if index < len(self.pieces):
            return self.pieces[index]
        else:
            return self.pieces[0]

    def get_next_piece(self, index):
        next_index = index + 1
        return self.get_piece(next_index)

    def get_next(self, piece):
        index = self.pieces.index(piece)
        return self.get_next_piece(index)

    def __str__(self):
        output = ''
        for piece in self.pieces:
            output += '{0} \n'.format(piece)
        return output

    def until_next_curve_start(self, car_position):
        piece = self.get_piece(car_position.piece_index)
        
        if (piece.is_curved == True):
            return 0

        untill_next_curve = piece.length - car_position.in_piece_distance 

        i = 1
        next_piece = self.get_piece(car_position.piece_index+i)

        while next_piece.is_curved == False:
            i = i + 1
            untill_next_curve = untill_next_curve + next_piece.length
            next_piece = self.get_piece(((car_position.piece_index+i) % len(self.pieces)))
            pass
        
        return untill_next_curve

    def does_lap_have_curves_left(self, car_position):
        piece = self.get_piece(car_position.piece_index)
        if (piece.is_curved == True):
            return True
        i = 1
        next_piece = self.get_piece(car_position.piece_index+i)
        while next_piece.is_curved == False :
            i = i + 1
            if(car_position.piece_index+i  > len(self.pieces)):
                return False
                break

            next_piece = self.get_piece(((car_position.piece_index+i)))
            pass

        if (car_position.piece_index + i > len(self.pieces)):
            return False

        return True

class CarHistory:
    def __init__(self):
        self.positions = []
        self.current_lap = 0

    def __str__(self):
        output = ''
        previous = self.positions[0] if self.positions else None
        for position in self.positions:
            output += '({0}, speed: {1} \n'.format(position, self.speed(previous, position))
            previous = position
        return output

    def add_position(self, position):
        self.positions.append(position)

    def speed(self, previous, current):
        if previous.piece_index == current.piece_index:
            return current.in_piece_distance - previous.in_piece_distance
        else:
            return 0

    def current_speed(self):
        positions_length = len(self.positions)
        if positions_length < 2:
            return 0
        else:
            return self.speed(self.positions[positions_length - 2], self.positions[positions_length - 1])

    def current_position(self):
        positions_length = len(self.positions)
        return self.positions[positions_length - 1]

class Turbo:
    def __init__(self, duration_ms, duration_ticks, factor):
        self.turboDurationMilliseconds = duration_ms
        self.turboDurationTicks = duration_ticks
        self.turboFactor = factor

    def __str__(self):
        return 'TurboInfo = DurationMilliseconds: {0}, DurationTics: {1}, Factor: {2}'.format(self.turboDurationMilliseconds, self.turboDurationTicks, self.turboFactor)