import math
import game_state

def set_throttle(car_position, current_speed, track, car_settings):
    piece = track.get_piece(car_position.piece_index)
    speed = car_settings.acceleration_speed
    next_curve_speed = estimate_next_curve_speed(piece, track, car_settings)
    distance_to_curve = distance_to_end(car_position, piece)
    braking_distance = minimum_distance_to_brake(current_speed, next_curve_speed, car_settings)

    if piece.is_curved:
        current_curve_speed = car_settings.curve_speed(piece.angle)
        if current_curve_speed < current_speed:
            speed = car_settings.brake_speed
        else:
            speed = current_curve_speed
    elif distance_to_curve <= braking_distance:
        speed = car_settings.brake_speed

    #print('current speed {0}, next curve speed {1}, distance to next curve {2}, min braking distance {3}, next speed {4}'.format(current_speed, next_curve_speed, distance_to_curve, braking_distance, speed))

    return car_settings.throttle_power(speed)

def distance_to_end(car_position, piece):
    return piece.length - car_position.in_piece_distance

def distance_to_next_curve(car_position, piece, track):
    next = piece
    distance = next.length
    while not next.is_curved:
        next = track.get_next(next)
        distance += next.length

    return distance - car_position.in_piece_distance

def estimate_next_curve_speed(piece, track, car_settings):
    next = piece
    while not next.is_curved:
        next = track.get_next(next)

    return car_settings.curve_speed(next.angle)

def minimum_distance_to_brake(speed, curve_speed, car_settings):
    if speed < curve_speed:
        return 0
    else:
        distance = (speed - curve_speed) * car_settings.braking_distance_factor
        return distance

def speed_close_to_target(target_speed, speed):
    return speed - target_speed < 0.2

def adjust_crashed_car_settings(current_piece, current_speed, car_settings):
    curve_speed = car_settings.curve_speed(current_piece.angle)
    if speed_close_to_target(curve_speed, current_speed):
        car_settings.decrease_angle_speed(current_piece.angle)
    else:
        car_settings.increase_braking_distance()

class CarSettings:
    def __init__(self):
        self.acceleration_speed = 10
        self.braking_distance = 50
        self.brake_speed = 0
        self.turbo_available = False
        self.turbo = game_state.Turbo(0.0,0,0.0)
        self.braking_distance_factor = 5

        self.angle45_speed = 9
        self.angle22_speed = 9

    def __str__(self):
        return 'braking distance: {0}'.format(self.braking_distance)

    def curve_speed(self, angle):
        abs_angle = math.fabs(angle)

        if abs_angle == 45:
            return self.angle45_speed
        elif abs_angle == 22:
            return self.angle22_speed
        else:
            return self.acceleration_speed

    def throttle_power(self, speed):
        return speed / 10.0

    def decrease_angle_speed(self, angle):
        abs_angle = math.fabs(angle)
        print('decreasing angle speed of angle {0}'.format(abs_angle))

        if abs_angle == 45:
            self.angle45_speed -= 1
        elif abs_angle == 22:
            self.angle22_speed -= 1

    def increase_braking_distance(self):
        print('increasing braking distance')
        self.braking_distance_factor += 5

    def decrease_braking_distance(self):
        print('increasing braking distance')
        if (self.braking_distance_factor > 0):
            self.braking_distance_factor -= 5