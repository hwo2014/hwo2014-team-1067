import json
import game_state

def read_track(data):
    # data = game_init_test_data()
    track = game_state.Track()

    pieces_data = data['race']['track']['pieces']
    for piece_data in pieces_data:
        piece = read_piece(piece_data)
        track.add_piece(piece)
    return track

def read_car_position(data, name):
    # data = car_position_test_data()
    car_position = game_state.CarPosition(0, 0.0, 0.0)

    for car_data in data:
        if (car_data['id']['name'] == name):
            car_position.angle = float(car_data.get('angle', '0.0'))
            position_data = car_data['piecePosition']
            car_position.piece_index = int(position_data.get('pieceIndex', '0'))
            car_position.in_piece_distance = float(position_data.get('inPieceDistance', '0.0'))
            break

    return car_position

def read_piece(piece_data):
    length = int(piece_data.get('length', '0'))
    angle = int(piece_data.get('angle', '0'))
    radius = int(piece_data.get('radius', '0'))
    piece = game_state.Piece(length, radius, angle)
    return piece

def get_crashed_car(data):
    return data['name']

def read_turbo(turbo_data):        
    duration_ms = float(turbo_data.get('turboDurationMilliseconds', '0.0'))
    duration_ticks = int(turbo_data.get('turboDurationTicks', '0.0'))
    factor = float(turbo_data.get('turboFactor', '0.0'))

    turbo = game_state.Turbo(duration_ms, duration_ticks, factor)
    return turbo

# functions for creating mock data
def game_init_test_data():
    test_data = '{"race": {"track": {"id": "indianapolis","name": "Indianapolis","pieces": [{"length": 100.0},{"length": 100.0,"switch": true},{"radius": 200,"angle": 22.5}],"lanes": [{"distanceFromCenter": -20,"index": 0},{"distanceFromCenter": 0,"index": 1},{"distanceFromCenter": 20,"index": 2}],"startingPoint": {"position": {"x": -340.0,"y": -96.0},"angle": 90.0}},"cars": [{"id": {"name": "Schumacher","color": "red"},"dimensions": {"length": 40.0,"width": 20.0,"guideFlagPosition": 10.0}},{"id": {"name": "Rosberg","color": "blue"},"dimensions": {"length": 40.0,"width": 20.0,"guideFlagPosition": 10.0}}],"raceSession": {"laps": 3,"maxLapTimeMs": 30000,"quickRace": true}}}'
    parsed_data = json.loads(test_data)
    return parsed_data

def car_position_test_data():
    test_data = '[{"id": {"name": "Schumacher", "color": "red" }, "angle": 0.0,"piecePosition": {"pieceIndex": 1,"inPieceDistance": 10.0,"lane": {"startLaneIndex": 0,"endLaneIndex": 0},"lap": 0}},{"id": {"name": "Rosberg","color": "blue"},"angle": 45.0,"piecePosition": {"pieceIndex": 0,"inPieceDistance": 20.0,"lane": {"startLaneIndex": 1,"endLaneIndex": 1},"lap": 0}}]'
    parsed_data = json.loads(test_data)
    return parsed_data